﻿using Newtonsoft.Json;
using PlayVideoExWeb.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace PlayVideoExWeb.Controllers
{
    public class HomeController : Controller
    {
        public static Guid _id = Guid.Empty;

        #region Upload File to API Root Directory and Save path to DB. Then get file urls
        public async Task<ActionResult> Index()
        {
            //List<string> filePaths = await GetRootFiles();
            //if (filePaths.Count > 0)
            //{
            //    ViewBag.Files = filePaths;
            //}
            return View();
        }

        [HttpGet]
        public async Task<List<string>> GetRootFiles()
        {
            List<string> m = new List<string>();
            var client = new HttpClient();
            var requestUri = "https://localhost:44324/api/upload";
            var result = await client.GetAsync(requestUri);
            if (result.StatusCode == System.Net.HttpStatusCode.OK)
            {
                m = await result.Content.ReadAsAsync<List<string>>();
            }
            else
            {
                ViewBag.Failed = "Failed !" + result.Content.ToString();
            }

            return m;
        }

        [HttpPost]
        public async Task<ActionResult> UploadVideoFile(HttpPostedFileBase file)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    using (var content = new MultipartFormDataContent())
                    {
                        byte[] Bytes = new byte[file.InputStream.Length + 1];
                        file.InputStream.Read(Bytes, 0, Bytes.Length);
                        var fileContent = new ByteArrayContent(Bytes);

                        Base64FormattingOptions opts = Base64FormattingOptions.None;
                        var b64str = Convert.ToBase64String(Bytes, opts);

                        fileContent.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment") { FileName = file.FileName };

                        var requestUri = "https://localhost:44324/api/upload";
                        content.Add(fileContent);
                        var result = await client.PostAsync(requestUri, content);
                        if (result.StatusCode == HttpStatusCode.Created)
                        {
                            List<string> m = await result.Content.ReadAsAsync<List<string>>();
                            ViewBag.Success = m;
                        }
                        else
                        {
                            ViewBag.Failed = "Failed !" + result.Content.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.Failed = ex.Message;
            }

            return View("Index");
        }

        [HttpPost]
        public async Task<ActionResult> UploadVideoFile2(HttpPostedFileBase file)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    byte[] Bytes = new byte[file.ContentLength];
                    using (BinaryReader theReader = new BinaryReader(file.InputStream))
                    {
                        Bytes = theReader.ReadBytes(file.ContentLength);
                    }
                    Base64FormattingOptions opts = Base64FormattingOptions.None;
                    var b64str = Convert.ToBase64String(Bytes, opts);
                    var httpEncodeStr = WebUtility.UrlEncode(b64str);

                    VideoFileModel model = new VideoFileModel();
                    model.Name = file.FileName;
                    model.FileStr = httpEncodeStr;
                    model.ContentType = file.ContentType;

                    string serailizeddto = JsonConvert.SerializeObject(model);
                    var requestUrl = "https://localhost:44324/api/savevideofile";
                    var requestUri = new Uri("https://localhost:44324/api/savevideofile");
                    var inputMessage = new HttpRequestMessage
                    {
                        Content = new StringContent(serailizeddto, Encoding.UTF8, "application/json")
                    };

                    inputMessage.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    var result = await client.PostAsync(requestUrl, inputMessage.Content);

                    if (result.StatusCode == HttpStatusCode.Created)
                    {
                        string m = await result.Content.ReadAsAsync<string>();
                        ViewBag.Success = m;
                    }
                    else
                    {
                        ViewBag.Failed = "Failed !" + result.Content.ToString();
                    }
                }
                return View("Index");
            }
            catch (Exception ex)
            {
                ViewBag.Failed = ex.StackTrace;
                return View("Index");
            }
        }

        #endregion


        #region Waste of Time
        public async Task<ActionResult> Index2()
        {
            var items = await GetByteFiles();
            IEnumerable<VideoFileModel> models = items; // (IEnumerable<VideoFileModel>)(IEnumerator<VideoFileModel>)
            return View(models);
        }

        public ActionResult Video(Guid id)
        {
            _id = id;
            return new VideoResult();
        }

        public async Task<ActionResult> PartialView(Guid id)
        {
            ViewBag.Id = id;
            var items = await GetByteFiles();
            IEnumerable<VideoFileModel> models = items; // (IEnumerable<VideoFileModel>)(IEnumerator<VideoFileModel>)
            return View("Index2", models);
        }

        [HttpGet]
        public async Task<List<VideoFileModel>> GetByteFiles()
        {
            List<VideoFileModel> m = new List<VideoFileModel>();
            var client = new HttpClient();
            var requestUri = "https://localhost:44324/api/getbytefiles";
            var result = await client.GetAsync(requestUri);
            if (result.StatusCode == System.Net.HttpStatusCode.OK)
            {
                m = await result.Content.ReadAsAsync<List<VideoFileModel>>();
            }
            else
            {
                ViewBag.Failed = "Failed !" + result.Content.ToString();
            }

            return m;
        }

        public async Task<Stream> GetFileByte(Guid id)
        {
            Stream content = Stream.Null;
            try
            {
                var client = new HttpClient();
                var requestUri = "https://localhost:44324/api/getbytefile/" + id;
                var result = await client.GetAsync(requestUri);
                if (result.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    content = await result.Content.ReadAsStreamAsync();
                }
                else
                {
                    ViewBag.Failed = "Failed !" + result.Content.ToString();
                }

            }
            catch (Exception ex)
            {
                ViewBag.Failed = "Failed !" + ex.Message;
            }

            return content;
        }


        [HttpPost]
        public async Task<ActionResult> UploadFile(HttpPostedFileBase file)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    using (var content = new MultipartFormDataContent())
                    {
                        byte[] Bytes = new byte[file.InputStream.Length + 1];
                        file.InputStream.Read(Bytes, 0, Bytes.Length);
                        var fileContent = new ByteArrayContent(Bytes);
                        fileContent.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment") { FileName = file.FileName };
                        content.Add(fileContent);
                        var requestUri = "https://localhost:44324/api/uploadfile";
                        var result = await client.PostAsync(requestUri, content);
                        if (result.StatusCode == System.Net.HttpStatusCode.Created)
                        {
                            List<string> m = await result.Content.ReadAsAsync<List<string>>();
                            ViewBag.Success = m;
                        }
                        else
                        {
                            ViewBag.Failed = "Failed !" + result.Content.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.Failed = ex.Message;
            }

            return View("DBFiles");
        }

        public class VideoResult : ActionResult
        {
            /// <summary>
            /// The below method will respond with the Video file
            /// </summary>
            /// <param name="context"></param>
            public async override void ExecuteResult(ControllerContext context)
            {
                if (_id == Guid.Empty) return;

                var client = new HttpClient();
                var requestUri = "https://localhost:44324/api/getbytefile/" + _id;
                var result = await client.GetAsync(requestUri);

                if (result.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var stream = await result.Content.ReadAsStreamAsync();
                    if (stream != Stream.Null)
                    {
                        var bytesinfile = new byte[stream.Length];
                        stream.Read(bytesinfile, 0, (int)stream.Length);
                        context.HttpContext.Response.BinaryWrite(bytesinfile);
                    }
                }

                //The header information
                //context.HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=Win8.mp4");
                //var file = new FileInfo(videoFilePath);
                ////Check the file exist,  it will be written into the response
                //if (file.Exists)
                //{
                //    var stream = file.OpenRead();
                //    var bytesinfile = new byte[stream.Length];
                //    stream.Read(bytesinfile, 0, (int)file.Length);
                //    context.HttpContext.Response.BinaryWrite(bytesinfile);
                //}
            }
        }

        #endregion

    }

}