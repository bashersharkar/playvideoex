﻿using Google.Analytics.Data.V1Beta;
using Google.Api.Gax;
using Google.Apis.Analytics.v3;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace PlayVideoExWeb.Controllers
{
    public class AnalyticsController : Controller
    {
        // GET: Analytics
        public async Task<ActionResult> Index()
        {
            var d = await GAnalyticsReport();
            return View();
        }

        public async Task<dynamic> GAnalyticsReport()
        {
            string propertyId = "247099974";
            string startDate = "2021-06-01";
            string endDate = "2021-06-17";

            // Add the  dimension

            try
            {
                /**
             * TODO(developer): Uncomment this variable and replace with your
             *  Google Analytics 4 property ID before running the sample.
             */
                // propertyId = "YOUR-GA4-PROPERTY-ID";

                // Using a default constructor instructs the client to use the credentials
                // specified in GOOGLE_APPLICATION_CREDENTIALS environment variable.
                BetaAnalyticsDataClient client = await BetaAnalyticsDataClient.CreateAsync();

                // Initialize request argument(s)
                RunReportRequest request = new RunReportRequest
                {
                    Property = "properties/" + propertyId,
                    Dimensions = {
                        new Dimension { Name = "itemId" },
                    },
                    Metrics = {
                        new Metric { Name = "customEvent:app_in_forground" },
                        new Metric { Name = "averageCustomEvent:app_in_forground" },
                        new Metric { Name = "countCustomEvent:app_in_forground" },
                    },
                    DateRanges = { new DateRange { StartDate = startDate, EndDate = endDate }, },
                    Limit = 50
                };

                // Make the request
                //PagedEnumerable<RunReportResponse, DimensionHeader>
                var response = await client.RunReportAsync(request);
                var netricHeader = response.MetricHeaders;
                var datas = response.Rows.ToList();
                return response;

                //foreach (RunReportResponse page in response.AsRawResponses())
                //{
                //    foreach (Row row in page.Rows)
                //    {
                //        Console.WriteLine("{0}, {1}", row.DimensionValues[0].Value, row.MetricValues[0].Value);
                //    }
                //}

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public dynamic GAnalyticsReport()
        //{
        //    string profileId = "ga:244734347";
        //    string startDate = "2021-06-13";
        //    string endDate = "2021-06-30";
        //    string metrics = "ga:visitors,ga:newVisits,ga:percentNewVisits";

        //    // Add the  dimension

        //    try
        //    {
        //        var serviceAccountEmail = "sampleproject@quickstart-1568957228218.iam.gserviceaccount.com";
        //        var certificate = new X509Certificate2(@"D:\quickstart-1568957228218-117e5077a16f.p12", "notasecret", X509KeyStorageFlags.Exportable);

        //        var credential = new ServiceAccountCredential(
        //        new ServiceAccountCredential.Initializer(serviceAccountEmail)
        //        {
        //            Scopes = new[] { AnalyticsService.Scope.Analytics }
        //        }.FromCertificate(certificate));

        //        // Create the service.
        //        //NopCommerce
        //        var GoogleAnalyticsService = new AnalyticsService(new BaseClientService.Initializer()
        //        {
        //            HttpClientInitializer = credential,
        //            ApplicationName = "sampleproject",
        //        });

        //        var request = GoogleAnalyticsService.Data.Ga.Get(profileId, startDate, endDate, metrics);

        //        //Specify some addition query parameters
        //        request.Sort = "-ga:visitors";
        //        request.MaxResults = 50;
        //        request.Dimensions = "ga:day,ga:country,ga:browser";
        //        request.Filters = "ga:country==Bangladesh";
        //        request.Segment = "users::condition::ga:browser==Chrome";
        //        request.StartIndex = 3;
        //        //Execute and fetch the results of our query
        //        Google.Apis.Analytics.v3.Data.GaData d = request.Execute();
        //        return d;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public dynamic GAnalyticsReport()
        //{
        //    string profileId = "ga:247099974";
        //    string startDate = "2021-06-13";
        //    string endDate = "2021-06-30";
        //    string metrics = "ga:visitors,ga:newVisits,ga:percentNewVisits";

        //    // Add the  dimension

        //    try
        //    {
        //        //var serviceAccountEmail = "retailerapp@retailerapp-blrtl019.iam.gserviceaccount.com";
        //        //var certificate = new X509Certificate2(@"C:\Users\bashe\Downloads\retailerapp-blrtl019-6f05a47dbb32.p12", "notasecret", X509KeyStorageFlags.Exportable);

        //        var serviceAccountEmail = "retailerapp-blrtl019@appspot.gserviceaccount.com";
        //        var certificate = new X509Certificate2(@"D:\retailerapp-blrtl019-d0532ba6c328.p12", "notasecret", X509KeyStorageFlags.Exportable);

        //        var credential = new ServiceAccountCredential(
        //        new ServiceAccountCredential.Initializer(serviceAccountEmail)
        //        {
        //            Scopes = new[] { AnalyticsService.Scope.Analytics }
        //        }.FromCertificate(certificate));

        //        // Create the service.
        //        //NopCommerce
        //        var GoogleAnalyticsService = new AnalyticsService(new BaseClientService.Initializer()
        //        {
        //            HttpClientInitializer = credential,
        //            ApplicationName = "App Engine default service account",
        //        });

        //        var request = GoogleAnalyticsService.Data.Ga.Get(profileId, startDate, endDate, metrics);

        //        //Specify some addition query parameters
        //        //request.Sort = "-ga:visitors";
        //        request.MaxResults = 100;
        //        //request.Dimensions = "ga:day,ga:country,ga:browser";
        //        //request.Filters = "ga:country==Bangladesh";
        //        //request.Segment = "users::condition::ga:browser==Chrome";
        //        request.StartIndex = 3;
        //        //Execute and fetch the results of our query
        //        Google.Apis.Analytics.v3.Data.GaData d = request.Execute();
        //        return d;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

    }
}