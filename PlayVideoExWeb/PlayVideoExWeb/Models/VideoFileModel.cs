﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PlayVideoExWeb.Models
{
    public class VideoFileModel
    {
        public VideoFileModel()
        {
            FileList = new List<VideoFileModel>();
        }

        public virtual Guid Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string ContentType { get; set; }
        public virtual byte[] ByteFile { get; set; }

        public virtual string FileStr { get; set; }

        public virtual List<VideoFileModel> FileList { get; set; }
    }
}