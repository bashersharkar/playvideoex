﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PlayVideoExAPI.Data
{
    public class VideoFile
    {
        [Key]
        public virtual Guid Id { get; set; }
        [Required]
        public virtual string Name { get; set; }
        public virtual string ContentType { get; set; }
        [Required]
        public virtual byte[] ByteFile { get; set; }
    }

    public class VideoFilePath
    {
        [Key]
        public virtual Guid Id { get; set; }
        [Required]
        public virtual string Name { get; set; }
        [Required]
        public virtual string FilePath { get; set; }
    }
}