﻿using PlayVideoExAPI.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace PlayVideoExAPI.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(HttpPostedFileBase file)
        {
            ViewBag.Title = "Home Page";
            try
            {
                if (file == null) return View();
                string b64Str = string.Empty;
                using (var client = new HttpClient())
                {
                    byte[] Bytes = new byte[file.ContentLength];
                    using (BinaryReader theReader = new BinaryReader(file.InputStream))
                    {
                        Bytes = theReader.ReadBytes(file.ContentLength);
                    }
                    Base64FormattingOptions opts = Base64FormattingOptions.None;
                    b64Str = Convert.ToBase64String(Bytes, opts);
                    ViewBag.Success = b64Str;
                }
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.Failed = ex.Message;
                return View();
            }
        }

        public ActionResult Index2(VideoFileModel model)
        {
            ViewBag.Title = "Home Page";
            var viewModel = new VideoFileModel();
            if (model != null) viewModel = model;
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Upload2(HttpPostedFileBase file)
        {
            VideoFileModel model = new VideoFileModel();
            try
            {
                if (file == null) return View();
                string httpEncodeStr = string.Empty;
                using (var client = new HttpClient())
                {
                    byte[] Bytes = new byte[file.ContentLength];
                    using (BinaryReader theReader = new BinaryReader(file.InputStream))
                    {
                        Bytes = theReader.ReadBytes(file.ContentLength);
                    }
                    Base64FormattingOptions opts = Base64FormattingOptions.None;
                    var b64str = Convert.ToBase64String(Bytes, opts);
                    httpEncodeStr = WebUtility.UrlEncode(b64str);
                    model.FileStr = httpEncodeStr;
                }
                return View("Index2", model);
            }
            catch (Exception ex)
            {
                ViewBag.Failed = ex.Message;
                return View("Index2", model);
            }
        }


    }
}
