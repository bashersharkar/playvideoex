﻿using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using PlayVideoExAPI.Data;
using PlayVideoExAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;

namespace PlayVideoExAPI.Controllers
{
    public class UploadController : ApiController
    {
        #region Upload file to Directory And Save Path to Database, Then Get File
        // GET: api/Upload
        public async Task<HttpResponseMessage> Get()
        {
            List<string> filePaths = new List<string>();
            try
            {
                //string rootPath = HostingEnvironment.ApplicationPhysicalPath;
                //string videoFolder = rootPath + "Videos";
                //string[] paths = Directory.GetFiles(videoFolder);

                AppDbContext _db = new AppDbContext();
                string query = "select * from VideoFilePath c order by c.Name";
                var files = await _db.Database.SqlQuery<VideoFilePath>(query).ToListAsync();

                //foreach (string path in paths)
                //{
                //    string newPath = path.Replace(videoFolder + "\\", "");
                //    Uri baseuri = new Uri(Request.RequestUri.AbsoluteUri.Replace(Request.RequestUri.PathAndQuery, string.Empty));
                //    string fileRelativePath = "~/Videos/" + newPath;
                //    Uri fileFullPath = new Uri(baseuri, VirtualPathUtility.ToAbsolute(fileRelativePath));

                //    filePaths.Add(fileFullPath.ToString());
                //}

                foreach (var file in files)
                {
                    Uri baseuri = new Uri(Request.RequestUri.AbsoluteUri.Replace(Request.RequestUri.PathAndQuery, string.Empty));
                    Uri fileFullPath = new Uri(baseuri, VirtualPathUtility.ToAbsolute("~" + file.FilePath));

                    filePaths.Add(fileFullPath.ToString());
                }
                return Request.CreateResponse(HttpStatusCode.OK, filePaths);
            }
            catch (Exception)
            {
                throw new HttpResponseException(HttpStatusCode.ExpectationFailed);
            }
        }

        // POST: api/upload
        //public async Task<HttpResponseMessage> Post()
        //{
        //    List<string> savedFilePath = new List<string>();
        //    string rootPath = HttpContext.Current.Server.MapPath("~/Videos");

        //    if (!Directory.Exists(rootPath))
        //    {
        //        Directory.CreateDirectory(rootPath);
        //    }

        //    try
        //    {
        //        if (!Request.Content.IsMimeMultipartContent())
        //        {
        //            throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
        //        }
        //        var provider = new MultipartFileStreamProvider(rootPath);
        //        var provider2 = new MultipartFormDataStreamProvider(rootPath);
        //        AppDbContext _db = new AppDbContext();

        //        var task = await Request.Content.ReadAsMultipartAsync(provider).ContinueWith(t =>
        //        {
        //            if (t.IsCanceled || t.IsFaulted)
        //            {
        //                Request.CreateErrorResponse(HttpStatusCode.InternalServerError, t.Exception);
        //            }

        //            foreach (MultipartFileData item in provider.FileData)
        //            {
        //                try
        //                {
        //                    string name = item.Headers.ContentDisposition.FileName.Replace("\"", "");
        //                    string newFileName = Guid.NewGuid() + Path.GetExtension(name);
        //                    File.Move(item.LocalFileName, Path.Combine(rootPath, newFileName));

        //                    Uri baseuri = new Uri(Request.RequestUri.AbsoluteUri.Replace(Request.RequestUri.PathAndQuery, string.Empty));
        //                    string fileRelativePath = "/Videos/" + newFileName;
        //                    Uri fileFullPath = new Uri(baseuri, VirtualPathUtility.ToAbsolute("~" + fileRelativePath));
        //                    savedFilePath.Add(fileFullPath.ToString());

        //                    VideoFilePath videoFile = new VideoFilePath
        //                    {
        //                        Id = Guid.NewGuid(),
        //                        Name = Path.GetFileName(name),
        //                        FilePath = fileRelativePath
        //                    };

        //                    _db.VideoFilePaths.Add(videoFile);
        //                }
        //                catch (Exception ex)
        //                {
        //                    string message = ex.Message;
        //                }
        //            }

        //            return Request.CreateResponse(HttpStatusCode.Created, savedFilePath);
        //        });

        //        await _db.SaveChangesAsync();

        //        return task;
        //    }
        //    catch (Exception ex)
        //    {
        //        return Request.CreateErrorResponse(HttpStatusCode.ExpectationFailed, ex.Message);
        //    }
        //}

        [HttpPost]
        [Route("api/savevideofile1")]
        public HttpResponseMessage PostVideoFile1([FromBody] VideoFileModel model)
        {
            try
            {
                if (model == null)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Failed");
                }
                if (string.IsNullOrEmpty(model.FileStr))
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Failed");
                }
                return Request.CreateResponse(HttpStatusCode.Created, "Success");
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.ExpectationFailed, ex.StackTrace);
            }
        }

        [HttpPost]
        [Route("api/savevideofile2")]
        public async Task<HttpResponseMessage> PostVideoFile2([FromBody] VideoFileModel model)
        {
            //AppDbContext _db = new AppDbContext();
            string rootPath = HttpContext.Current.Server.MapPath("~/Videos");

            if (!Directory.Exists(rootPath))
            {
                Directory.CreateDirectory(rootPath);
            }

            try
            {
                VideoFilePath dataModel = new VideoFilePath
                {
                    Id = Guid.NewGuid(),
                    Name = model.Name,
                };

                string base64data = Regex.Replace(model.FileStr, @"^data:image\/[a-zA-Z]+;base64,", string.Empty);
                var fileData = Convert.FromBase64String(base64data);

                string newFileName = Guid.NewGuid() + ".mp4";// Path.GetExtension(model.Name);
                FileInfo fil = new FileInfo(Path.Combine(rootPath, newFileName));
                using (Stream sw = fil.OpenWrite())
                {
                    await sw.WriteAsync(fileData, 0, fileData.Length);
                    sw.Close();
                }

                string fileRelativePath = "/Videos/" + newFileName;
                Uri baseuri = new Uri(Request.RequestUri.AbsoluteUri.Replace(Request.RequestUri.PathAndQuery, string.Empty));
                Uri fileFullPath = new Uri(baseuri, VirtualPathUtility.ToAbsolute("~" + fileRelativePath));

                if (model == null)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Failed");
                }
                if (string.IsNullOrEmpty(model.FileStr))
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Failed");
                }
                return Request.CreateResponse(HttpStatusCode.Created, fileFullPath.ToString());
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.ExpectationFailed, ex.StackTrace);
            }
        }

        [HttpPost]
        [Route("api/savevideofile")]
        public async Task<HttpResponseMessage> PostVideoFile([FromBody] VideoFileModel model)
        {
            //AppDbContext _db = new AppDbContext();
            string rootPath = HttpContext.Current.Server.MapPath("~/Videos");

            if (!Directory.Exists(rootPath))
            {
                Directory.CreateDirectory(rootPath);
            }

            try
            {
                VideoFilePath dataModel = new VideoFilePath
                {
                    Id = Guid.NewGuid(),
                    Name = model.Name,
                };

                var httpUrlDecode = WebUtility.UrlDecode(model.FileStr);
                string base64data = Regex.Replace(httpUrlDecode, @"^data:image\/[a-zA-Z]+;base64,", string.Empty);
                var fileData = Convert.FromBase64String(base64data);

                string newFileName = Guid.NewGuid() + ".mp4";// Path.GetExtension(model.Name);
                FileInfo fil = new FileInfo(Path.Combine(rootPath, newFileName));
                using (Stream sw = fil.OpenWrite())
                {
                    await sw.WriteAsync(fileData, 0, fileData.Length);
                    sw.Close();
                }

                string fileRelativePath = "/Videos/" + newFileName;
                Uri baseuri = new Uri(Request.RequestUri.AbsoluteUri.Replace(Request.RequestUri.PathAndQuery, string.Empty));
                Uri fileFullPath = new Uri(baseuri, VirtualPathUtility.ToAbsolute("~" + fileRelativePath));

                if (model == null)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Failed");
                }
                if (string.IsNullOrEmpty(model.FileStr))
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Failed");
                }
                return Request.CreateResponse(HttpStatusCode.Created, fileFullPath.ToString());
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.ExpectationFailed, ex.StackTrace);
            }
        }

        #endregion


        //[HttpGet]
        //[Route("api/getbytefiles")]
        //public async Task<HttpResponseMessage> GetByteFiles()
        //{
        //    try
        //    {
        //        AppDbContext _db = new AppDbContext();
        //        string query = "select * from VideoFile c order by c.Name";
        //        var files = await _db.Database.SqlQuery<VideoFile>(query).ToListAsync();
        //        return Request.CreateResponse(HttpStatusCode.OK, files);
        //    }
        //    catch (Exception ex)
        //    {
        //        return Request.CreateErrorResponse(HttpStatusCode.ExpectationFailed, ex.Message);
        //    }
        //}


        //[HttpGet]
        //[Route("api/getbytefile/id")]
        //public async Task<HttpResponseMessage> GetByteFile(Guid id)
        //{
        //    try
        //    {
        //        //Create HTTP Response.
        //        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);

        //        //Fetch the File data from Database.
        //        AppDbContext _db = new AppDbContext();
        //        VideoFile file = await _db.VideoFiles.FindAsync(id);

        //        //Set the Response Content.
        //        byte[] _content = Encoding.UTF8.GetBytes(file.ByteFile.ToString());
        //        response.Content = new ByteArrayContent(file.ByteFile);
        //        MemoryStream memStream = new MemoryStream(_content);

        //        //Set the Response Content Length.
        //        response.Content.Headers.ContentLength = file.ByteFile.LongLength;

        //        //Set the Content Disposition Header Value and FileName.
        //        response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
        //        {
        //            FileName = file.Name
        //        };

        //        //Set the File Content Type.
        //        response.Content.Headers.ContentType = new MediaTypeHeaderValue(file.ContentType);
        //        return response;
        //    }
        //    catch (Exception ex)
        //    {
        //        return Request.CreateErrorResponse(HttpStatusCode.ExpectationFailed, ex.Message);
        //    }
        //}


        //[HttpPost]
        //[Route("api/uploadfile")]
        //public async Task<HttpResponseMessage> UploadByteFile()
        //{
        //    try
        //    {
        //        if (!Request.Content.IsMimeMultipartContent())
        //        {
        //            throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
        //        }
        //        //Check if Request contains File.
        //        if (HttpContext.Current.Request.Files.Count == 0)
        //        {
        //            throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
        //        }

        //        //Read the File data from Request.Form collection.
        //        HttpPostedFile postedFile = HttpContext.Current.Request.Files[0];

        //        //Convert the File data to Byte Array.
        //        byte[] bytes;
        //        using (BinaryReader br = new BinaryReader(postedFile.InputStream))
        //        {
        //            bytes = br.ReadBytes(postedFile.ContentLength);
        //        }

        //        //Insert the File to Database Table.
        //        AppDbContext _db = new AppDbContext();
        //        VideoFile videoFile = new VideoFile
        //        {
        //            Id = Guid.NewGuid(),
        //            Name = Path.GetFileName(postedFile.FileName),
        //            ContentType = GetMimeType(postedFile.FileName),
        //            ByteFile = bytes
        //        };

        //        _db.VideoFiles.Add(videoFile);
        //        await _db.SaveChangesAsync();

        //        return Request.CreateResponse(HttpStatusCode.Created, new { id = videoFile.Id, Name = videoFile.Name });
        //    }
        //    catch (Exception ex)
        //    {
        //        return Request.CreateErrorResponse(HttpStatusCode.ExpectationFailed, ex.Message);
        //    }
        //}

        //private string GetMimeType(string fileName)
        //{
        //    string mimeType = "application/unknown";
        //    string ext = Path.GetExtension(fileName).ToLower();
        //    Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
        //    if (regKey != null && regKey.GetValue("Content Type") != null)
        //        mimeType = regKey.GetValue("Content Type").ToString();
        //    return mimeType;
        //}

    }
}
