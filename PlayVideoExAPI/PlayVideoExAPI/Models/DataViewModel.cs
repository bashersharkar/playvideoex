﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PlayVideoExAPI.Models
{
    public class VideoFileModel
    {
        public virtual Guid Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string ContentType { get; set; }
        public virtual byte[] ByteFile { get; set; }
        [Required]
        public virtual string FileStr { get; set; }
    }
}